# A very simple Flask Hello World app for you to get started with...

from flask import Flask, redirect, url_for, render_template, request, send_from_directory
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select, PrimaryKey
import keygen
from forms import LoginForm

import os

app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


db = Database()
class People(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    category = Required(str)
    email = Required(str)
    telephone = Optional(int)
    address = Optional(str)
    education = Optional(str)
    work = Optional(str)
    skills = Optional(str)
    language = Optional(str)



class User(UserMixin, db.Entity): #same as class User(db.Entity)://UserMixin extend login class (inheritance)

    username = Required(str, unique=True)
    password_hash = Optional(str)

    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)



@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))




@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return redirect(url_for('index'))

@app.route('/logout',methods=['GET' , 'POST'])
@login_required
def logout():
    logout_user()
    return redirect('/')




@app.route('/',methods=['GET' , 'POST'])
@login_required
@db_session
def index():
    if request.method == 'GET':

        searched_term=request.args.get('category','')
        find = list(select(t for t in People if searched_term.upper() in t.name.upper()))
        return render_template('index.html',USERS = find,NAME=current_user.username)

    elif request.method == 'POST':
        return redirect(url_for('fill'))




@app.route('/delete/<id>', methods=['GET','POST'])
@db_session
@login_required
def delete(id):
    if People[id]:
        People[id].delete()
        return redirect(url_for('index'))


@app.route('/<id>', methods=['GET' , 'POST'])
@db_session
@login_required
def update(id):
        if request.method == 'GET':
            if People[id]:
                return render_template('individual.html',USERS = People[id])
        elif request.method == 'POST':
            if People[id]:
                n = request.form.get('name')
                c= request.form.get('category')
                e= request.form.get('email')
                t = request.form.get('telephone')
                a= request.form.get('address')
                ed= request.form.get('education')
                w = request.form.get('work')
                s= request.form.get('skills')
                l= request.form.get('language')

                People[id].set(name=n, category=c,  telephone=t, email=e, address=a, education=ed, work=w, skills=s, language=l)

                return redirect(url_for('index'))

            return 'This product does not exist'

@app.route('/post',methods=['GET' , 'POST'])
@db_session
@login_required
def fill():
    if request.method == 'GET':

        return render_template('post.html',NAME=current_user.username)
    elif request.method == 'POST':
        People (name= request.form.get('name'),
                category = request.form.get('category'),
                email = request.form.get('email'),
                telephone = request.form.get('telephone'),
                address = request.form.get('address'),
                education = request.form.get('education'),
                work = request.form.get('work'),
                skills = request.form.get('skills'),
                language = request.form.get('language'))
        return redirect(url_for('index'))



@app.route("/upload")
def upld():
    return render_template("upload.html")

@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT, 'images/')
    # target = os.path.join(APP_ROOT, 'static/')
    print(target)
    if not os.path.isdir(target):
            os.mkdir(target)
    else:
        print("Couldn't create upload directory: {}".format(target))
    print(request.files.getlist("file"))
    for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "/".join([target, filename])
        print ("Accept incoming file:", filename)
        print ("Save it to:", destination)
        upload.save(destination)

    # return send_from_directory("images", filename, as_attachment=True)
    return render_template("complete.html", image_name=filename)

@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images/", filename)


if __name__ == '__main__':
    app.run(threaded=True, port=5000)

